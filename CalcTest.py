__author__ = 'AndresFelipe'

import unittest
from Calc import Calc


class CalcTestCase(unittest.TestCase):
    def test_suma_vacio(self):
        self.assertEqual(Calc().suma(""), 0, "String Vacio")

    def test_suma_un_numero(self):
        self.assertEqual(Calc().suma("1"), 1, "String Un Numero")


if __name__ == '__main__':
    unittest.main()
